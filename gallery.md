# Gallery

## Left half with a Pimoroni PIM 447 track ball

![Left Yackboard v1](v1/images/yackboard-left.jpg)

## Right half with an Adafruit 444 thumb joystick

![Right Yackboard v1](v1/images/yackboard-right.jpg)

## The PCBs

![PCB v1 top](v1/images/pcb-1.jpg)
![PCB v1 botton](v1/images/pcb-2.jpg)

## Back of the keyboards

![Left Yackboard v1 bottom](v1/images/yackboard-left-2.jpg)
![Right Yackboard v1 bottom](v1/images/yackboard-right-2.jpg)

## Mounting of the PCB and the covers

![Detail 1](v1/images/yackboard-detail-1.jpg)
![Detail 2](v1/images/yackboard-detail-2.jpg)
