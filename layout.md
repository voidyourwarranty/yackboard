# The OPY-Layout

An ortholinear layout in the tradition of [NEO2](https://www.neo-layout.org/Layouts/), [AdNW](http://www.adnw.de/) and
[KOY](https://www.maximilian-schillinger.de/keyboard-layouts-neo-adnw-koy.html) for a keyboard with shift and space on
the thumbs, optimized for mixed English and German usage and paying careful attention to same-finger bigrams, to their
frequencies and locations. The name `OPY` derives from the top left row: 'QZOPY'.

If you dislike same-finger bigrams on your weak fingers, then `OPY` may be worth considering. `OPY` is strongly
inspired by the criteria that shaped [KOY](https://www.maximilian-schillinger.de/keyboard-layouts-neo-adnw-koy.html),
but transferred from the standard row-staggered ISO layout to an ortholinear one with one 3x5 purely alphabetical block
under each hand. `OPY` is a direct competitor to
[BuT](http://www.adnw.de/index.php?n=Main.OptimierungF%C3%BCrDieGeradeTastaturMitDaumen-Shift).

Using the [Online Keyboard Layout Editor](http://www.keyboard-layout-editor.com) I have drawn the layout as follows:

![Layout](v1/layout/yackboard-v1.jpg)

The layout de-emphasizes the corners, emphasizes the home positions of all fingers, encourages hand change, and pays a
lot of attention to same finger bigrams, how often and where they occur. In the above diagram, the black letters on the
top left of the keys show the alphabetical layer that was obtained from a custom optimizer.

For the alphabetical layer, please refer to the web site of the [OPY
Layout](https://opylayout.wordpress.com/2023/01/21/the-opy-layout/).

## Physical Properties

The keyboard is split into two independent halves, one for each hand, that are connected by Bluetooth Low Energy (BLE)
with each other and with the computer. I follow the philosophy that no finger other than the thumbs ought to move
further than one position away from its home position. This limits the keyboard, apart from the thumbs, to three rows of
six columns for each hand. The index and the pinkie finger may serve two columns while the middle and ring fingers serve
one column each. Since the pinkies are the weakest, the current version does not use the second column of the pinkies at
all. The thumbs, however, are among the strongest and fastest fingers and also the most mobile, and so they get five key
positions each.

## Logical Properties

### Layer and Hand Structure

As with 2x (3x5 + 5) = 40 keys, there is no space for a number row nor for cursor keys nor for a function key row, these
are added in separate layers. For each of these groups, it makes sense to concentrate the most relevant keys on the
right half of the keyboard so that it is possible to remove the left hand from the keyboard in some situations:
- entry of numbers and basic formulas while retaining basic editing functions
- full navigation and basic editing functions

### Special Keys

On a regular US ANSI or European ISO keyboard, many of the most frequently pressed keys are located under the pinkie
fingers: shift, control, backspace and enter. It makes sense to move all of them to the thumbs which are under-utilized
by the standard keyboard layouts.

For the thumbs, I place shift and the main layer switches under the left thumb whereas space, enter, backspace and tab
are under the right thumb. This leaves all relevant editing keys on the right half so that the keyboard may be used
single-handedly for navigation.

### Alphabetical keys

The layout of the default layer (black annotation towards the left) contains precisely the alphabetic keys, i.e. the 26
letters and the 4 German 'Umlaut' and 'ß' symbols. All punctuation has been moved to a special layer. The alphabetic
layout is described on the web site of the [OPY Layout](https://opylayout.wordpress.com/2023/01/21/the-opy-layout/).

### Modifier keys

On small keyboards, so called 'home row mods' are popular. I have decided to use a dedicated shift key to avoid issues
with fast typing, but Control, (left) Alt and Win are implemented as 'home row mods'. For more details on home row mods,
please take a look at precondition's [guide](https://precondition.github.io/home-row-mods). The modifiers obtained by
holding down keys are indicated on the front of the keys. Another good reference on the use of layers and modifiers is
the [Miryoku Layout](https://github.com/manna-harbour/miryoku/tree/master/docs/reference).

Holding the dedicated shift key is the shift modifier. Tapping it once is a one-shot shift (sticky shift). Tapping it
twice is 'caps word', i.e. capitalizes the subsequent word. Holding shift without tapping any other key resets the layer
to default.

### Layers

The layer keys L-1 to L-3 switch to the number and symbols layer, to the navigation layer, and to the function key
layer, respectively. Their key bindings are indicated in blue, red, and green, respectively. The layer keys on the left
half can be tapped once in order to switch to the layer for the subsequent key only ('one-shot layer' or 'sticky
layer'), tapped twice in order to switch to that layer or be held down to activate the layer temporarily. Afterwards,
all non-default layers are deactivated.

The enter, space and backspace keys on the right half can be held down in order to temporarily switch to the numbers and
symbols and the navigation layer as well as to activate shift, too, in order to relieve the left thumb. This way of
layer switching is similar to the Miryoku layout.

In contrast to the Miryoku layout, however,
- there are fewer chords because shift and layer can be navigated by tapping and not only by holding
- the number pad, the navigation keys, and the function keys are all concentrated on the right half and thanks to the
  five thumb keys, the right half can be used alone for a number of tasks

In order to be able to use the number and the navigation functions with the right hand only, there is a 'layer cycle key'
L-next that, if tapped, goes to the next layer, and if held down, goes back to the default layer.

The thumb keys have got the same function in all layers except for the space key which is the number zero on the number
layer.

In order to further enhance the right half of the number layer, the minus key serves as an apostrophe if held, the
asterisk as a caret, and the decimal point as a comma.

### ZMK Implementation

In order to use this layout with the [ZMK
firmware](https://gitlab.com/voidyourwarranty/zmk-yackboard/-/tree/devel-mouse-pim447), you need to select the 'US
International' keyboard layout in your operating system.
