# Evaluation of version 1

Version 1 suffers from the following rookie errors on the mechanical side. Luckily the electrical wiring turned out to
be correct,
- The distance between the PCB and the upper acrylic plate is a maximum of 1mm, better just one washer thick, but this
  may not be sufficient to solder the pin header that's on the bottom of the PCB.
- The spacers for the cover of the microcontroller are too close to the upper acrylic cover to add an M2.5 nut. I was lucky
  that my spacers still fit in.
- One of the drill holes for mounting a cover of the Cirque touch pad is partly covered by the upper acrylic cover.
- The distance of the drill holes for mounting the Adafruit thumb joystick is wrong (too small). With 6mm spacers,
  however, it just works.
- The spacers under the PCB (6mm) are largely filled by the screws of the standoffs for the cover of the
  microcontroller. As a consequence the 4mm screws from the bottom do not fit in. I am now trying 3mm screws.
- The distance between two spacers should not be larger than, say, 80mm, i.e. at the back of the keyboard, I ought to
  add one. Not all of the others are required.
- The wires of my preconfigured battery cables have red and black confused (or my batteries have).
