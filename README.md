# Yackboard ([Y]et [A]nother [C]ustom [K]ey Board)

The Yackboard is a wireless custom ergonomic split keyboard designed using open source software and produced with the
help of not too expensive standardized production services.

## Version 1

Highlights:
- Layout design using [ergogen](https://www.github.com/ergogen/ergogen) by Dénes Bán with this specific
  [configuration](https://gitlab.com/voidyourwarranty/ergogen-yackboard).
- Kailh Choc low profile switches with hot swap sockets using the smaller choc spacing as opposed to the more common MX
  spacing.
- PCB routed using [KiCAD](https://www.kicad.org/) 6.0.2.
- [Nice!Nano](https://nicekeyboards.com/docs/nice-nano/) micro-controller with Bluetooth Low Energy (BLE) for each half
  of the keyboard.
- [ZMK](https://www.github.com/zmkfirmware/zmk) firmware by Pete Johanson and the ZMK community with these specific
  [patches](https://gitlab.com/voidyourwarranty/zmk-yackboard/-/tree/devel-mouse-pim447).
- There is ample space for mounting and trying various pointing devices including a cut-out in the PCB for their custom
  wiring as well as a 10-pin header for standard DuPont jumper cables at the bottom of the PCB that exposes 3V3, GND and
  the 8 unused DIL pins of the Nice!Nano micro-controller.

Note that the ZMK Firmware is still experimental, and if you want more than just the keys, you will need to tweak ZMK.

The pictures show the left half with a [Pimoroni Pim 447 Track
Ball](https://shop.pimoroni.com/products/trackball-breakout?variant=27672765038675) and the right half with an [Adafruit
444 Thumb Joystick](https://www.adafruit.com/product/444).

![Left Yackboard v1](v1/images/yackboard-left.jpg)
![Right Yackboard v1](v1/images/yackboard-right.jpg)

## Bill of Materials

- 2 PCBs produced from these [Gerber files](v1/Gerber). For example, [JLCPCB](https://jlcpcb.com/) in Hong Kong/Shenzhen
  accepted and produced them without any issues (2-layer PCB, FR4 material, black solder mask, white silk screen on both
  sides, ENIG coating).
- 2 top covers (3mm thick), 2 bottom covers (any thickness) and 2 micro-controller covers (any thickness) laser cut in
  these [shapes](v1/acrylic) from any suitable material. For example, [Sculpteo](http://www.sculpteo.com) produced them
  without any issues as follows: top covers in 3mm glossy opaque black acrylic and all other parts in 1mm transparent
  acrylic.
- 46 Kailh Choc how swap sockets
- 46 SMD diodes 1N4148 with 1206 footprint
- 46 Kailh Choc switches with key caps for choc spacing (36 key caps 1u wide plus 10 caps 1.5u wide)
- 2 reset buttons for 2-pin through-hole mounting (2.54mm spacing)
- 2 power switches for 3-pin through-hole mounting (2.54mm spacing)
- 2 10-pin angled pin headers (2.54mm spacing)
- 2 rechargeable 3.7V LiPo batteries with wires soldered to the PCB. I found 500 mAh batteries that are only 4mm thick
  and that fit into the 6mm space between the PCB and the bottom cover. They came with a 2-pin JST-PH 2.0 connector, and
  I soldered pre-configured cables with the corresponding JST-PH 2.0 sockets to the PCB.
- plenty of M2.5 screws, washers, nuts and spacers in order to assemble the parts. I have 6mm spacers (continuous
  thread) between the PCB and the bottom cover and 15mm spacers (one screw, one thread) between the PCB and the cover of
  the micro-controller and merely 0.2mm washers between the PCB and the top cover.

## Topics

- [Gallery](gallery.md)
- [Layout](layout.md)
- [Evaluation of Version 1](version1.md)
